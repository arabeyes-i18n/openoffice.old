# translation of tbxctrls.po to Arabic
# extracted from svx/source/tbxctrls.oo
# Ossama M. Khayat <okhayat@yahoo.com>, 2004, 2005.
msgid ""
msgstr ""
"Project-Id-Version: tbxctrls\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2005-08-11 20:32+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#: colrctrl.src#RID_SVXCTRL_COLOR.dockingwindow.text
msgid "Colors"
msgstr "الألوان"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_PERSPECTIVE.string.text
msgid "~Perspective"
msgstr "المن~ظور"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_PARALLEL.string.text
msgid "P~arallel"
msgstr "توا~زي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_NW.string.text
msgid "Extrusion North-West"
msgstr "نتوء شمالي-غربي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_N.string.text
msgid "Extrusion North"
msgstr "نتوء شمالي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_NE.string.text
msgid "Extrusion North-East"
msgstr "نتوء شمالي-شرقي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_W.string.text
msgid "Extrusion West"
msgstr "نتوء غربي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_NONE.string.text
msgid "Extrusion Backwards"
msgstr "نتوء للخلف"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_E.string.text
msgid "Extrusion East"
msgstr "نتوء شرقي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_SW.string.text
msgid "Extrusion South-West"
msgstr "نتوء جنوبي-غربي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_S.string.text
msgid "Extrusion South"
msgstr "نتوء جنوبي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.STR_DIRECTION___DIRECTION_SE.string.text
msgid "Extrusion South-East"
msgstr "نتوء جنوبي-شرقي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DIRECTION.floatingwindow.text
msgid "Extrusion Direction"
msgstr "اتجاه النتوء"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DEPTH.STR_CUSTOM.string.text fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.STR_CHARS_SPACING_CUSTOM.string.text
msgid "~Custom..."
msgstr "~مخصص..."

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DEPTH.STR_INFINITY.string.text
msgid "~Infinity"
msgstr "ما لا ن~هاية"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_DEPTH.floatingwindow.text extrusioncontrols.src#RID_SVX_MDLG_EXTRUSION_DEPTH.modaldialog.text
msgid "Extrusion Depth"
msgstr "عمق النتوء"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_LIGHTING.STR_BRIGHT.string.text
msgid "~Bright"
msgstr "ن~اصع"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_LIGHTING.STR_NORMAL.string.text fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.STR_CHARS_SPACING_NORMAL.string.text
msgid "~Normal"
msgstr "~عادي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_LIGHTING.STR_DIM.string.text
msgid "~Dim"
msgstr "~لامع"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_LIGHTING.floatingwindow.text
msgid "Extrusion Lighting"
msgstr "إضاءة النتوء"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_SURFACE.STR_WIREFRAME.string.text
msgid "~Wire Frame"
msgstr "إ~طار سلكي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_SURFACE.STR_MATTE.string.text
msgid "~Matt"
msgstr "~جاف"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_SURFACE.STR_PLASTIC.string.text
msgid "~Plastic"
msgstr "~بلاستيكي"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_SURFACE.STR_METAL.string.text
msgid "Me~tal"
msgstr "م~عدني"

#: extrusioncontrols.src#RID_SVXFLOAT_EXTRUSION_SURFACE.floatingwindow.text
msgid "Extrusion Surface"
msgstr "سطح النتوء"

#: extrusioncontrols.src#RID_SVX_MDLG_EXTRUSION_DEPTH.FL_DEPTH.fixedtext.text fontworkgallery.src#RID_SVX_MDLG_FONTWORK_CHARSPACING.FT_VALUE.fixedtext.text
msgid "~Value"
msgstr "ال~قيمة"

#: extrusioncontrols.src#RID_SVXSTR_EXTRUSION_COLOR.string.text
msgid "Extrusion Color"
msgstr "لون النتوء"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_0.string.text
msgid "~0 cm"
msgstr "~0 سم"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_1.string.text
msgid "~1 cm"
msgstr "~1 سم"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_2.string.text
msgid "~2.5 cm"
msgstr "~2.5 سم"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_3.string.text
msgid "~5 cm"
msgstr "~5 سم"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_4.string.text
msgid "10 ~cm"
msgstr "10 ~سم"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_0_INCH.string.text
msgid "0 inch"
msgstr "0 إنش"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_1_INCH.string.text
msgid "0.~5 inch"
msgstr "0.~5 إنش"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_2_INCH.string.text
msgid "~1 inch"
msgstr "~1 إنش"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_3_INCH.string.text
msgid "~2 inch"
msgstr "~2 إنش"

#: extrusioncontrols.src#RID_SVXSTR_DEPTH_4_INCH.string.text
msgid "~4 inch"
msgstr "~4 إنش"

#: fontworkgallery.src#RID_SVX_MDLG_FONTWORK_GALLERY.FL_FAVORITES.fixedline.text
msgid "Select a Fontwork style:"
msgstr "اختيار نمط Fontwork:"

#: fontworkgallery.src#RID_SVX_MDLG_FONTWORK_GALLERY.STR_CLICK_TO_ADD_TEXT.string.text
msgid "Click to edit text"
msgstr "اضغط لتحرير النص"

#: fontworkgallery.src#RID_SVX_MDLG_FONTWORK_GALLERY.modaldialog.text
msgid "Fontwork Gallery"
msgstr "معرض Fontwork"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_ALIGNMENT.STR_ALIGN_LEFT.string.text
msgid "~Left Align"
msgstr "~محاذاة لليسار"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_ALIGNMENT.STR_ALIGN_CENTER.string.text
msgid "~Center"
msgstr "تو~سيط"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_ALIGNMENT.STR_ALIGN_RIGHT.string.text
msgid "~Right Align"
msgstr "محاذاة للي~مين"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_ALIGNMENT.STR_ALIGN_WORD.string.text
msgid "~Word Justify"
msgstr "ضبط ال~كلمات"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_ALIGNMENT.STR_ALIGN_STRETCH.string.text
msgid "S~tretch Justify"
msgstr "ضب~ط المدّ"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_ALIGNMENT.floatingwindow.text
msgid "Fontwork Alignment"
msgstr "محاذاة Fontwork"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.STR_CHARS_SPACING_VERY_TIGHT.string.text
msgid "~Very Tight"
msgstr "متقارب ~جداً"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.STR_CHARS_SPACING_TIGHT.string.text
msgid "~Tight"
msgstr "مت~قارب"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.STR_CHARS_SPACING_LOOSE.string.text
msgid "~Loose"
msgstr "متبا~عد"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.STR_CHARS_SPACING_VERY_LOOSE.string.text
msgid "Very ~Loose"
msgstr "متبا~عد جداً"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.STR_CHARS_SPACING_KERN_PAIRS.string.text
msgid "~Kern Character Pairs"
msgstr "ت~قنين أزواج الأحرف"

#: fontworkgallery.src#RID_SVXFLOAT_FONTWORK_CHARSPACING.floatingwindow.text fontworkgallery.src#RID_SVX_MDLG_FONTWORK_CHARSPACING.modaldialog.text
msgid "Fontwork Character Spacing"
msgstr "تباعد أحرف Fontwork"

#: grafctrl.src#RID_SVXTBX_GRFFILTER.floatingwindow.text
msgid "Filters"
msgstr "مُصفّيات"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFMODE.string.text
msgid "Graphics Mode"
msgstr "وضع الرسوم"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFRED.string.text
msgid "Red"
msgstr "أحمر"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFGREEN.string.text
msgid "Green"
msgstr "أخضر"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFBLUE.string.text
msgid "Blue"
msgstr "أزرق"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFLUMINANCE.string.text
msgid "Brightness"
msgstr "الإضاءة"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFCONTRAST.string.text
msgid "Contrast"
msgstr "التباين"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFGAMMA.string.text
msgid "Gamma"
msgstr "غاما"

#: grafctrl.src#RID_SVXSTR_UNDO_GRAFTRANSPARENCY.string.text
msgid "Transparency"
msgstr "الشفافية"

#: grafctrl.src#RID_SVXSTR_GRAFCROP.string.text
msgid "Crop"
msgstr "اقتصاص"

#: lboxctrl.src#RID_SVXSTR_NUM_UNDO_ACTIONS.string.text
msgid "Undo $(ARG1) actions"
msgstr "التراجع عن $(ARG1) أعمال"

#: lboxctrl.src#RID_SVXSTR_NUM_UNDO_ACTION.string.text
msgid "Undo $(ARG1) action"
msgstr "التراجع عن $(ARG1) عمل"

#: lboxctrl.src#RID_SVXSTR_NUM_REDO_ACTIONS.string.text
msgid "Redo $(ARG1) actions"
msgstr "إعادة $(ARG1) أعمال"

#: lboxctrl.src#RID_SVXSTR_NUM_REDO_ACTION.string.text
msgid "Redo $(ARG1) action"
msgstr "إعادة $(ARG1) عمل"

#: tbcontrl.src#RID_SVXSTR_TRANSPARENT.string.text
msgid "No Fill"
msgstr "بدون تعبئة"

#: tbcontrl.src#RID_SVXSTR_FILLPATTERN.string.text
msgid "Pattern"
msgstr "نقش"

#: tbcontrl.src#RID_SVXSTR_FRAME.string.text
msgid "Borders"
msgstr "الحدود"

#: tbcontrl.src#RID_SVXSTR_FRAME_STYLE.string.text
msgid "Border Style"
msgstr "نمط الحدود"

#: tbcontrl.src#RID_SVXSTR_FRAME_COLOR.string.text
msgid "Border Color"
msgstr "لون الحدود"

#: tbcontrl.src#RID_SVXSTR_EXTRAS_CHARBACKGROUND.string.text
msgid "Highlighting"
msgstr "تعليم"

#: tbcontrl.src#RID_SVXSTR_BACKGROUND.string.text
msgid "Background"
msgstr "الخلفية"

#: tbcontrl.src#RID_SVXSTR_AUTOMATIC.string.text
msgid "Automatic"
msgstr "تلقائي"

#: tbcontrl.src#RID_SVXSTR_PAGES.string.text
msgid "Pages"
msgstr "صفحات"

#: tbcontrl.src#RID_SVXSTR_CLEARFORM.string.text
msgid "Clear formatting"
msgstr "مسح التنسيق"

#: tbcontrl.src#RID_SVXSTR_MORE.string.text
msgid "More..."
msgstr "المزيد..."

#: tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.1.itemlist.text tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.6.itemlist.text
msgid "Default"
msgstr "افتراضي"

#: tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.2.itemlist.text tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.7.itemlist.text
msgid "Heading 1"
msgstr "ترويسة 1"

#: tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.3.itemlist.text tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.8.itemlist.text
msgid "Heading 2"
msgstr "ترويسة 2"

#: tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.4.itemlist.text
msgid "Heading 3"
msgstr "ترويسة 3"

#: tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.5.itemlist.text
msgid "Text body"
msgstr "متن النص"

#: tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.9.itemlist.text
msgid "Result"
msgstr "النتيجة"

#: tbcontrl.src#RID_SVXSTRARRAY_DEFAULTSTYLES.10.itemlist.text
msgid "Result2"
msgstr "النتيجة2"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_OBJECT_SELECT.toolboxitem.text
msgid "Selection"
msgstr "التحديد"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_LINE.toolboxitem.text
msgid "Line"
msgstr "خط"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_RECT.toolboxitem.text
msgid "Rectangle"
msgstr "مستطيل"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_ELLIPSE.toolboxitem.text
msgid "Ellipse"
msgstr "بيضوي"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_POLYGON_NOFILL.toolboxitem.text
msgid "Polygon"
msgstr "مضلّع"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_BEZIER_NOFILL.toolboxitem.text
msgid "Curve"
msgstr "منحنى"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_FREELINE_NOFILL.toolboxitem.text
msgid "Freeform Line"
msgstr "منحنى حر"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_ARC.toolboxitem.text
msgid "Arc"
msgstr "قوس"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_PIE.toolboxitem.text
msgid "Ellipse Pie"
msgstr "قطاع دائرة"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_CIRCLECUT.toolboxitem.text
msgid "Circle Segment"
msgstr "قطاع دائرة"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_TEXT.toolboxitem.text
msgid "Text"
msgstr "النص"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_TEXT_VERTICAL.toolboxitem.text
msgid "Vertical Text"
msgstr "نص عمودي"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_TEXT_MARQUEE.toolboxitem.text
msgid "Text Animation"
msgstr "تحريك النص"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_CAPTION.toolboxitem.text
msgid "Callouts"
msgstr "تعليق تفسيري"

#: tbxdraw.src#RID_SVXTBX_DRAW.TBX_DRAW.SID_DRAW_CAPTION_VERTICAL.toolboxitem.text
msgid "Vertical Callout"
msgstr "تعليق تفسيري عمودي"

#: tbxdraw.src#RID_SVXTBX_DRAW.floatingwindow.text
msgid "Draw Functions"
msgstr "وظائف رسم"

#: tbxdraw.src#RID_SVXTBX_ALIGNMENT.TBX_ALIGNMENT.SID_OBJECT_ALIGN_LEFT.toolboxitem.text
msgid "Left"
msgstr "يسار"

#: tbxdraw.src#RID_SVXTBX_ALIGNMENT.TBX_ALIGNMENT.SID_OBJECT_ALIGN_CENTER.toolboxitem.text
msgid "Center"
msgstr "وسط"

#: tbxdraw.src#RID_SVXTBX_ALIGNMENT.TBX_ALIGNMENT.SID_OBJECT_ALIGN_RIGHT.toolboxitem.text
msgid "Right"
msgstr "يمين"

#: tbxdraw.src#RID_SVXTBX_ALIGNMENT.TBX_ALIGNMENT.SID_OBJECT_ALIGN_UP.toolboxitem.text
msgid "Top"
msgstr "أعلى"

#: tbxdraw.src#RID_SVXTBX_ALIGNMENT.TBX_ALIGNMENT.SID_OBJECT_ALIGN_MIDDLE.toolboxitem.text
msgid "Centered"
msgstr "توسيط"

#: tbxdraw.src#RID_SVXTBX_ALIGNMENT.TBX_ALIGNMENT.SID_OBJECT_ALIGN_DOWN.toolboxitem.text
msgid "Bottom"
msgstr "أسفل"

#: tbxdraw.src#RID_SVXTBX_ALIGNMENT.floatingwindow.text
msgid "Alignment"
msgstr "محاذاة"

