# translation of items.po to Arabic
# extracted from svx/source/items.oo
# Ossama M. Khayat <okhayat@yahoo.com>, 2004, 2005.
msgid ""
msgstr ""
"Project-Id-Version: items\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2005-09-27 21:19+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"

#: svxerr.src#RID_SVXERRCTX.ERRCTX_SVX_LINGU_THESAURUS_ERRCODE_RES_MASK.string.text
msgid "$(ERR) executing the thesaurus."
msgstr "$(ERR) أثناء تنفيذ قاموس المرادفات."

#: svxerr.src#RID_SVXERRCTX.ERRCTX_SVX_LINGU_SPELLING_ERRCODE_RES_MASK.string.text
msgid "$(ERR) executing the spellcheck."
msgstr "$(ERR) أثناء تنفيذ عملية التدقيق الإملائي."

#: svxerr.src#RID_SVXERRCTX.ERRCTX_SVX_LINGU_HYPHENATION_ERRCODE_RES_MASK.string.text
msgid "$(ERR) executing the hyphenation."
msgstr "$(ERR) أثناء تنفيذ عملية فصل المقاطع."

#: svxerr.src#RID_SVXERRCTX.ERRCTX_SVX_LINGU_DICTIONARY_ERRCODE_RES_MASK.string.text
msgid "$(ERR) creating a dictionary."
msgstr "$(ERR) أثناء إنشاء قاموس."

#: svxerr.src#RID_SVXERRCTX.ERRCTX_SVX_BACKGROUND_ERRCODE_RES_MASK.string.text
msgid "$(ERR) setting background attribute."
msgstr "$(ERR) أثناء تعيين سمة الخلفية."

#: svxerr.src#RID_SVXERRCTX.ERRCTX_SVX_IMPORT_GRAPHIC_ERRCODE_RES_MASK.string.text
msgid "$(ERR) loading the graphics."
msgstr "$(ERR) أثناء تحميل الرسوم."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_LINGU_THESAURUSNOTEXISTS_ERRCODE_RES_MASK.string.text
msgid "No thesaurus available for the current language.\\nPlease check your installation and install the desired language"
msgstr "ليس هناك قاموس مرادفات متوفّر للغة الحالية.\\nالرجاء التحقق من التثبيت وتثبيت اللغة المطلوبة"

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_LINGU_LANGUAGENOTEXISTS_ERRCODE_RES_MASK.string.text
msgid ""
"$(ARG1) is not supported by the spellcheck function or is not presently active.\\nPlease check your installation and, if necessary, install the required language module\\n or activate it under "
"'Tools - Options -  Language Settings - Writing Aids'."
msgstr "$(ARG1) غير مدعوم من قبل وظيفة التدقيق الإملائي أو غير فاعل حالياً.\\nالرجاء التحقق من التثبيت وتثبيت وحدة اللغة المطلوبة إن كان ضروروياً\\n أو تفعيلها من خلال 'أدوات - خيارات -  إعدادات اللغة - مساعدات الكتابة'."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_LINGU_LINGUNOTEXISTS_ERRCODE_RES_MASK.string.text
msgid "Spellcheck is not available."
msgstr "التدقيق الإملائي غير متوفر."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_LINGU_HYPHENNOTEXISTS_ERRCODE_RES_MASK.string.text
msgid "Hyphenation not available."
msgstr "فصل المقاطع غير متوفر."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_LINGU_DICT_NOTREADABLE_ERRCODE_RES_MASK.string.text
msgid "The custom dictionary $(ARG1) cannot be read."
msgstr "لا يمكن قراءة القاموس الشخصي $(ARG1)."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_LINGU_DICT_NOTWRITEABLE_ERRCODE_RES_MASK.string.text
msgid "The custom dictionary $(ARG1) cannot be created."
msgstr "لا يمكن إنشاء القاموس الشخصي $(ARG1)."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_GRAPHIC_NOTREADABLE_ERRCODE_RES_MASK.string.text
msgid "The graphic $(ARG1) could not be found."
msgstr "تعذر العثور على الرسم $(ARG1)."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_GRAPHIC_WRONG_FILEFORMAT_ERRCODE_RES_MASK.string.text
msgid "An unlinked graphic could not be loaded"
msgstr "تعذر تحميل إحدى الصور الغير مرتبطة"

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_LINGU_NOLANGUAGE_ERRCODE_RES_MASK.string.text
msgid "A language has not been fixed for the selected term."
msgstr "لم يتم تثبيت لغة للمصطلح المحدد."

#: svxerr.src#RID_SVXERRCODE._ERRCODE_SVX_FORMS_NOIOSERVICES___ERRCODE_CLASS_READ____ERRCODE_RES_MASK.string.text
msgid "The form layer wasn't loaded as the required IO-services (stardiv.uno.io.*) could not be instantiated"
msgstr ""

#: svxerr.src#RID_SVXERRCODE._ERRCODE_SVX_FORMS_NOIOSERVICES___ERRCODE_CLASS_WRITE____ERRCODE_RES_MASK.string.text
msgid "The form layer wasn't written as the required IO services (stardiv.uno.io.*) could not be instantiated"
msgstr ".*)"

#: svxerr.src#RID_SVXERRCODE._ERRCODE_SVX_FORMS_READWRITEFAILED___ERRCODE_CLASS_READ____ERRCODE_RES_MASK.string.text
msgid "An error occurred while reading the form controls. The form layer has not been loaded"
msgstr "حدث خطأ أثناء قراءة عناصر تحكم الاستمارة. لم يتم تحميل طبقة الاستمارة"

#: svxerr.src#RID_SVXERRCODE._ERRCODE_SVX_FORMS_READWRITEFAILED___ERRCODE_CLASS_WRITE____ERRCODE_RES_MASK.string.text
msgid "An error occurred while writing the form controls. The form layer has not been saved"
msgstr "حدث خطأ أثناء كتابة عناصر تحكم الاستمارة. لم يتم حفظ طبقة الاستمارة"

#: svxerr.src#RID_SVXERRCODE._ERRCODE_SVX_BULLETITEM_NOBULLET___ERRCODE_CLASS_READ____ERRCODE_RES_MASK.string.text
msgid "An error occurred while reading one of the bullets. Not all of the bullets were loaded."
msgstr "حدث خطأ أثناء قراءة إحدى النقاط. تعذر تحميل كل علامات التعداد."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_MODIFIED_VBASIC_STORAGE___ERRCODE_RES_MASK.string.text
msgid "All changes to the Basic Code are lost.The original VBA Macro Code is saved instead."
msgstr "سوف تضيع كافة التعديلات التي تم إدخالها على نص Basic البرمجي. سيتم حفظ نص ماكرو VBA البرمجي بدلاً منه."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_VBASIC_STORAGE_EXIST___ERRCODE_RES_MASK.string.text
msgid "The original VBA Basic Code contained in the document will not be saved."
msgstr "لن يتم حفظ نص VBA Basic البرمجي الأصلي الموجود في المستند."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_WRONGPASS___ERRCODE_RES_MASK.string.text
msgid "The password is incorrect. The document cannot be opened."
msgstr "كلمة المرور غير صحيحة. لا يمكن فتح المستند."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_READ_FILTER_CRYPT___ERRCODE_RES_MASK.string.text
msgid "The encryption method used in this document is not supported. Only Microsoft Office 97/2000 compatible password encryption is supported."
msgstr "طريقة التشفير المستخدمة في هذا المستند غير مدعومة. فقط تشفير كلمة المرور المتوافق مع ميكروسوفت أوفيس 97/2000 compatible هو المدعوم."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_READ_FILTER_PPOINT___ERRCODE_RES_MASK.string.text
msgid "The loading of password-encrypted Microsoft PowerPoint presentations is not supported."
msgstr "تحميل عروض ميكروسوفت باوروبوينت المحمية بكلمة مرور مشفّرة غير مدعوم."

#: svxerr.src#RID_SVXERRCODE.ERRCODE_SVX_EXPORT_FILTER_CRYPT___ERRCODE_RES_MASK.string.text
msgid "Password protection is not supported when documents are saved in a Microsoft Office format.\\nDo you want to save the document without password protection"
msgstr "الحماية بكلمة مرور غير مدعوم عند حفظ المستندات بنسق ميكروسوفت أوفيس.\\nهل تريد حفظ المستند دون الحماية بكلمة مرور"

#: svxitems.src#RID_SVXITEMS_TRUE.string.text
msgid "True"
msgstr "صح"

#: svxitems.src#RID_SVXITEMS_FALSE.string.text
msgid "False"
msgstr "خطأ"

#: svxitems.src#RID_SVXITEMS_EXTRAS_CHARCOLOR.string.text svxitems.src#RID_ATTR_CHAR_COLOR.string.text
msgid "Font color"
msgstr "لون الخط"

#: svxitems.src#RID_SVXITEMS_SEARCHCMD_FIND.string.text
msgid "Search"
msgstr "بحث"

#: svxitems.src#RID_SVXITEMS_SEARCHCMD_FIND_ALL.string.text
msgid "Find All"
msgstr "بحث عن الكل"

#: svxitems.src#RID_SVXITEMS_SEARCHCMD_REPLACE.string.text
msgid "Replace"
msgstr "استبدال"

#: svxitems.src#RID_SVXITEMS_SEARCHCMD_REPLACE_ALL.string.text
msgid "Replace all"
msgstr "استبدال الكل"

#: svxitems.src#RID_SVXITEMS_SEARCHSTYL_CHAR.string.text
msgid "Character Style"
msgstr "نمط الأحرف"

#: svxitems.src#RID_SVXITEMS_SEARCHSTYL_PARA.string.text
msgid "Paragraph Style"
msgstr "نمط الفقرة"

#: svxitems.src#RID_SVXITEMS_SEARCHSTYL_FRAME.string.text
msgid "Frame Style"
msgstr "نمط إطار"

#: svxitems.src#RID_SVXITEMS_SEARCHSTYL_PAGE.string.text svxitems.src#RID_ATTR_PARA_MODEL.string.text
msgid "Page Style"
msgstr "نمط صفحة"

#: svxitems.src#RID_SVXITEMS_SEARCHIN_FORMULA.string.text
msgid "Formula"
msgstr "معادلة"

#: svxitems.src#RID_SVXITEMS_SEARCHIN_VALUE.string.text
msgid "Value"
msgstr "القيمة"

#: svxitems.src#RID_SVXITEMS_SEARCHIN_NOTE.string.text
msgid "Note"
msgstr "ملاحظة"

#: svxitems.src#RID_SVXITEMS_BREAK_NONE.string.text
msgid "No break"
msgstr "بدون فاصل"

#: svxitems.src#RID_SVXITEMS_BREAK_COLUMN_BEFORE.string.text
msgid "Break before new column"
msgstr "فاصل قبل العمود الجديد"

#: svxitems.src#RID_SVXITEMS_BREAK_COLUMN_AFTER.string.text
msgid "Break after new column"
msgstr "فاصل بعد العمود الجديد"

#: svxitems.src#RID_SVXITEMS_BREAK_COLUMN_BOTH.string.text
msgid "Break before and after new column"
msgstr "فاصل قبل وبعد العمود الجديد"

#: svxitems.src#RID_SVXITEMS_BREAK_PAGE_BEFORE.string.text
msgid "Break before new page"
msgstr "فاصل قبل الصفحة الجديدة"

#: svxitems.src#RID_SVXITEMS_BREAK_PAGE_AFTER.string.text
msgid "Break after new page"
msgstr "فاصل بعد الصفحة الجديدة"

#: svxitems.src#RID_SVXITEMS_BREAK_PAGE_BOTH.string.text
msgid "Break before and after new page"
msgstr "فاصل قبل وبعد الصفحة الجديدة"

#: svxitems.src#RID_SVXITEMS_SHADOW_NONE.string.text
msgid "No Shadow"
msgstr "بدون ظل"

#: svxitems.src#RID_SVXITEMS_SHADOW_TOPLEFT.string.text
msgid "Shadow top left"
msgstr "الظل أعلى اليسار"

#: svxitems.src#RID_SVXITEMS_SHADOW_TOPRIGHT.string.text
msgid "Shadow top right"
msgstr "الظل أعلى اليمين"

#: svxitems.src#RID_SVXITEMS_SHADOW_BOTTOMLEFT.string.text
msgid "Shadow bottom left"
msgstr "الظل أسفل اليسار"

#: svxitems.src#RID_SVXITEMS_SHADOW_BOTTOMRIGHT.string.text
msgid "Shadow bottom right"
msgstr "الظل أسفل اليمين"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_NULL.string.text svxitems.src#RID_SVXITEMS_CASEMAP_NONE.string.text svxitems.src#RID_SVXITEMS_PAGE_NUM_NONE.string.text
msgid "None"
msgstr "بدون"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_SOLID.string.text
#, fuzzy
msgid "Solid"
msgstr "ممتلئ"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_HORZ.string.text
msgid "Horizontal"
msgstr "أفقي"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_VERT.string.text
msgid "Vertical"
msgstr "رأسي"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_CROSS.string.text
msgid "Grid"
msgstr "الشبكة"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_DIAGCROSS.string.text
msgid "Diamond"
msgstr "مُعّين"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_UPDIAG.string.text
msgid "Diagonal up"
msgstr "قطري لأعلى"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_DOWNDIAG.string.text
msgid "Diagonal down"
msgstr "قطري لأسفل"

#: svxitems.src#RID_SVXITEMS_BRUSHSTYLE_BITMAP.string.text
msgid "Image"
msgstr "صورة"

#: svxitems.src#RID_SVXITEMS_COLOR.string.text
msgid "Color "
msgstr "اللون "

#: svxitems.src#RID_SVXITEMS_COLOR_BLACK.string.text
msgid "Black"
msgstr "أسود"

#: svxitems.src#RID_SVXITEMS_COLOR_BLUE.string.text
msgid "Blue"
msgstr "أزرق"

#: svxitems.src#RID_SVXITEMS_COLOR_GREEN.string.text
msgid "Green"
msgstr "أخضر"

#: svxitems.src#RID_SVXITEMS_COLOR_CYAN.string.text
msgid "Cyan"
msgstr "أزرق سماوي"

#: svxitems.src#RID_SVXITEMS_COLOR_RED.string.text
msgid "Red"
msgstr "أحمر"

#: svxitems.src#RID_SVXITEMS_COLOR_MAGENTA.string.text
msgid "Magenta"
msgstr "أرجواني"

#: svxitems.src#RID_SVXITEMS_COLOR_BROWN.string.text
msgid "Brown"
msgstr "بني"

#: svxitems.src#RID_SVXITEMS_COLOR_GRAY.string.text
msgid "Gray"
msgstr "رمادي"

#: svxitems.src#RID_SVXITEMS_COLOR_LIGHTGRAY.string.text
msgid "Light Gray"
msgstr "رمادي فاتح"

#: svxitems.src#RID_SVXITEMS_COLOR_LIGHTBLUE.string.text
msgid "Light Blue"
msgstr "أزرق فاتح"

#: svxitems.src#RID_SVXITEMS_COLOR_LIGHTGREEN.string.text
msgid "Light Green"
msgstr "أخضر فاتح"

#: svxitems.src#RID_SVXITEMS_COLOR_LIGHTCYAN.string.text
msgid "Light Cyan"
msgstr "سماوي فاتح"

#: svxitems.src#RID_SVXITEMS_COLOR_LIGHTRED.string.text
msgid "Light Red"
msgstr "أحمر فاتح"

#: svxitems.src#RID_SVXITEMS_COLOR_LIGHTMAGENTA.string.text
msgid "Light Magenta"
msgstr "أرجواني فاتح"

#: svxitems.src#RID_SVXITEMS_COLOR_YELLOW.string.text
msgid "Yellow"
msgstr "أصفر"

#: svxitems.src#RID_SVXITEMS_COLOR_WHITE.string.text
msgid "White"
msgstr "أبيض"

#: svxitems.src#RID_SVXITEMS_ITALIC_NONE.string.text
msgid "Not Italic"
msgstr "غير مائل"

#: svxitems.src#RID_SVXITEMS_ITALIC_OBLIQUE.string.text
msgid "Oblique italic"
msgstr "مائل منحدر"

#: svxitems.src#RID_SVXITEMS_ITALIC_NORMAL.string.text
msgid "Italic"
msgstr "مائل"

#: svxitems.src#RID_SVXITEMS_WEIGHT_THIN.string.text
msgid "thin"
msgstr "رفيع"

#: svxitems.src#RID_SVXITEMS_WEIGHT_ULTRALIGHT.string.text
msgid "ultra thin"
msgstr "رفيع للغاية"

#: svxitems.src#RID_SVXITEMS_WEIGHT_LIGHT.string.text
msgid "light"
msgstr "خفيف"

#: svxitems.src#RID_SVXITEMS_WEIGHT_SEMILIGHT.string.text
msgid "semi light"
msgstr "نصف خفيف"

#: svxitems.src#RID_SVXITEMS_WEIGHT_NORMAL.string.text
msgid "normal"
msgstr "عادي"

#: svxitems.src#RID_SVXITEMS_WEIGHT_MEDIUM.string.text
msgid "medium"
msgstr "متوسط"

#: svxitems.src#RID_SVXITEMS_WEIGHT_SEMIBOLD.string.text
msgid "semi bold"
msgstr "نصف عريض"

#: svxitems.src#RID_SVXITEMS_WEIGHT_BOLD.string.text
msgid "bold"
msgstr "عريض"

#: svxitems.src#RID_SVXITEMS_WEIGHT_ULTRABOLD.string.text
msgid "ultra bold"
msgstr "عريض للغاية"

#: svxitems.src#RID_SVXITEMS_WEIGHT_BLACK.string.text
msgid "black"
msgstr "أسود"

#: svxitems.src#RID_SVXITEMS_UL_NONE.string.text
msgid "No underline"
msgstr "بدون تسطير"

#: svxitems.src#RID_SVXITEMS_UL_SINGLE.string.text
msgid "Single underline"
msgstr "تسطير مفرد"

#: svxitems.src#RID_SVXITEMS_UL_DOUBLE.string.text
msgid "Double underlne"
msgstr "تسطير مزدوج"

#: svxitems.src#RID_SVXITEMS_UL_DOTTED.string.text
msgid "Dotted underline"
msgstr "تسطير منقط"

#: svxitems.src#RID_SVXITEMS_UL_DONTKNOW.string.text svxitems.src#RID_ATTR_CHAR_UNDERLINE.string.text
msgid "Underline"
msgstr "تسطير"

#: svxitems.src#RID_SVXITEMS_UL_DASH.string.text
msgid "Underline (dashes)"
msgstr "تسطير (شُرَط)"

#: svxitems.src#RID_SVXITEMS_UL_LONGDASH.string.text
msgid "Underline (long dashes)"
msgstr "تسطير (شُرَط طويلة)"

#: svxitems.src#RID_SVXITEMS_UL_DASHDOT.string.text
msgid "Underline (dot dash)"
msgstr "تسطير (نقطة شرطة)"

#: svxitems.src#RID_SVXITEMS_UL_DASHDOTDOT.string.text
msgid "Underline (dot dot dash)"
msgstr "تسطير (نقطة نقطة شرطة)"

#: svxitems.src#RID_SVXITEMS_UL_SMALLWAVE.string.text
msgid "Underline (small wave)"
msgstr "تسطير (موجة صغيرة)"

#: svxitems.src#RID_SVXITEMS_UL_WAVE.string.text
msgid "Underline (Wave)"
msgstr "تسطير (موجة)"

#: svxitems.src#RID_SVXITEMS_UL_DOUBLEWAVE.string.text
msgid "Underline (Double wave)"
msgstr "تسطير (موجة مزدوجة)"

#: svxitems.src#RID_SVXITEMS_UL_BOLD.string.text
msgid "Underlined (Bold)"
msgstr "تسطير (عريض)"

#: svxitems.src#RID_SVXITEMS_UL_BOLDDOTTED.string.text
msgid "Dotted underline (Bold)"
msgstr "تسطير منقط (عريض)"

#: svxitems.src#RID_SVXITEMS_UL_BOLDDASH.string.text
msgid "Underline (Dash bold)"
msgstr "تسطير (شرطة عريض)"

#: svxitems.src#RID_SVXITEMS_UL_BOLDLONGDASH.string.text
msgid "Underline (long dash, bold)"
msgstr "تسطير (شرطة طويلة، عريض)"

#: svxitems.src#RID_SVXITEMS_UL_BOLDDASHDOT.string.text
msgid "Underline (dot dash, bold)"
msgstr "تسطير (نقطة شرطة، عريض)"

#: svxitems.src#RID_SVXITEMS_UL_BOLDDASHDOTDOT.string.text
msgid "Underline (dot dot dash, bold)"
msgstr "تسطير (نقطة نقطة شرطة، عريض)"

#: svxitems.src#RID_SVXITEMS_UL_BOLDWAVE.string.text
msgid "Underline (wave, bold)"
msgstr "تسطير (موجة، عريض)"

#: svxitems.src#RID_SVXITEMS_STRIKEOUT_NONE.string.text
msgid "No strikethrough"
msgstr "غير مشطوب"

#: svxitems.src#RID_SVXITEMS_STRIKEOUT_SINGLE.string.text
msgid "Single strikethrough"
msgstr "مشطوب مفرد"

#: svxitems.src#RID_SVXITEMS_STRIKEOUT_DOUBLE.string.text
msgid "Double strikethrough"
msgstr "مشطوب مزدوج"

#: svxitems.src#RID_SVXITEMS_STRIKEOUT_BOLD.string.text
msgid "Bold strikethrough"
msgstr "مشطوب عريض"

#: svxitems.src#RID_SVXITEMS_STRIKEOUT_SLASH.string.text
msgid "Strike through with slash"
msgstr "مشطوب بشرط مائلة"

#: svxitems.src#RID_SVXITEMS_STRIKEOUT_X.string.text
msgid "Strike through with Xes"
msgstr "مشطوب بعلامات X"

#: svxitems.src#RID_SVXITEMS_CASEMAP_VERSALIEN.string.text
msgid "Caps"
msgstr "أحرف كبيرة"

#: svxitems.src#RID_SVXITEMS_CASEMAP_GEMEINE.string.text svxitems.src#RID_SVXITEMS_PAGE_NUM_CHR_LOWER.string.text
msgid "Lowercase"
msgstr "أحرف صغيرة"

#: svxitems.src#RID_SVXITEMS_CASEMAP_TITEL.string.text
msgid "Title"
msgstr "العنوان"

#: svxitems.src#RID_SVXITEMS_CASEMAP_KAPITAELCHEN.string.text
msgid "Small caps"
msgstr "أحرف بداية أكبر"

#: svxitems.src#RID_SVXITEMS_ESCAPEMENT_OFF.string.text
msgid "Normal position"
msgstr "موضع عادي"

#: svxitems.src#RID_SVXITEMS_ESCAPEMENT_SUPER.string.text
msgid "Superscript "
msgstr "مرتفع "

#: svxitems.src#RID_SVXITEMS_ESCAPEMENT_SUB.string.text
msgid "Subscript "
msgstr "منخفض "

#: svxitems.src#RID_SVXITEMS_ESCAPEMENT_AUTO.string.text
msgid "automatic"
msgstr "تلقائي"

#: svxitems.src#RID_SVXITEMS_ADJUST_LEFT.string.text svxitems.src#RID_SVXITEMS_HORJUST_LEFT.string.text
msgid "Align left"
msgstr "محاذاة إلى اليسار"

#: svxitems.src#RID_SVXITEMS_ADJUST_RIGHT.string.text svxitems.src#RID_SVXITEMS_HORJUST_RIGHT.string.text
msgid "Align right"
msgstr "محاذاة إلى اليمين"

#: svxitems.src#RID_SVXITEMS_ADJUST_BLOCK.string.text svxitems.src#RID_SVXITEMS_ADJUST_BLOCKLINE.string.text svxitems.src#RID_SVXITEMS_HORJUST_BLOCK.string.text
msgid "Justify"
msgstr "ضبط"

#: svxitems.src#RID_SVXITEMS_ADJUST_CENTER.string.text svxitems.src#RID_SVXITEMS_TAB_ADJUST_CENTER.string.text
msgid "Centered"
msgstr "توسيط"

#: svxitems.src#RID_SVXITEMS_DBTYPE_FLAT.string.text
msgid "Text"
msgstr "النص"

#: svxitems.src#RID_SVXITEMS_TAB_DECIMAL_CHAR.string.text
msgid "Decimal Symbol:"
msgstr "علامة عشرية:"

#: svxitems.src#RID_SVXITEMS_TAB_FILL_CHAR.string.text
msgid "Fill character:"
msgstr ""

#: svxitems.src#RID_SVXITEMS_TAB_ADJUST_LEFT.string.text svxitems.src#RID_SVXITEMS_PAGE_USAGE_LEFT.string.text
msgid "Left"
msgstr "يسار"

#: svxitems.src#RID_SVXITEMS_TAB_ADJUST_RIGHT.string.text svxitems.src#RID_SVXITEMS_PAGE_USAGE_RIGHT.string.text
msgid "Right"
msgstr "يمين"

#: svxitems.src#RID_SVXITEMS_TAB_ADJUST_DECIMAL.string.text
msgid "Decimal"
msgstr "عشرية"

#: svxitems.src#RID_SVXITEMS_TAB_ADJUST_DEFAULT.string.text
msgid "Default"
msgstr "افتراضي"

#: svxitems.src#RID_ATTR_ZOOM.string.text svxitems.src#RID_ATTR_CHAR_SCALEWIDTH.string.text
msgid "Scale"
msgstr "مقياس"

#: svxitems.src#RID_ATTR_BRUSH.string.text
msgid "Brush"
msgstr "فرشاة"

#: svxitems.src#RID_ATTR_TABSTOP.string.text
msgid "Tab stops"
msgstr "علامات الجدولة"

#: svxitems.src#RID_ATTR_CHAR.string.text
msgid "Character"
msgstr "حرف"

#: svxitems.src#RID_ATTR_CHAR_FONT.string.text
msgid "Font"
msgstr "الخط"

#: svxitems.src#RID_ATTR_CHAR_POSTURE.string.text
msgid "Font posture"
msgstr "وضع الخط"

#: svxitems.src#RID_ATTR_CHAR_WEIGHT.string.text
msgid "Font weight"
msgstr "عرض الخط"

#: svxitems.src#RID_ATTR_CHAR_SHADOWED.string.text svxitems.src#RID_SVXITEMS_SHADOWED_TRUE.string.text
msgid "Shadowed"
msgstr "مظلل"

#: svxitems.src#RID_ATTR_CHAR_WORDLINEMODE.string.text svxitems.src#RID_SVXITEMS_WORDLINE_TRUE.string.text
msgid "Individual words"
msgstr "الكلمات فقط"

#: svxitems.src#RID_ATTR_CHAR_CONTOUR.string.text svxitems.src#RID_SVXITEMS_CONTOUR_TRUE.string.text
msgid "Outline"
msgstr "تقسيم"

#: svxitems.src#RID_ATTR_CHAR_STRIKEOUT.string.text
msgid "Strikethrough"
msgstr "شطب"

#: svxitems.src#RID_ATTR_CHAR_FONTHEIGHT.string.text
msgid "Font size"
msgstr "حجم الخط"

#: svxitems.src#RID_ATTR_CHAR_PROPSIZE.string.text
msgid "Rel. Font Size"
msgstr "الحجم النسبي للخط"

#: svxitems.src#RID_ATTR_CHAR_KERNING.string.text
msgid "Kerning"
msgstr "تقنين الأحرف"

#: svxitems.src#RID_ATTR_CHAR_CASEMAP.string.text
msgid "Effects"
msgstr "التأثيرات"

#: svxitems.src#RID_ATTR_CHAR_LANGUAGE.string.text
msgid "Language"
msgstr "اللغة"

#: svxitems.src#RID_ATTR_CHAR_ESCAPEMENT.string.text
msgid "Position"
msgstr "الموضع"

#: svxitems.src#RID_ATTR_CHAR_BLINK.string.text svxitems.src#RID_ATTR_FLASH.string.text svxitems.src#RID_SVXITEMS_BLINK_TRUE.string.text
msgid "Blinking"
msgstr "وميض"

#: svxitems.src#RID_ATTR_CHAR_CHARSETCOLOR.string.text
msgid "Character set color"
msgstr "لون مجموعة الأحرف"

#: svxitems.src#RID_ATTR_PARA.string.text
msgid "Paragraph"
msgstr "الفقرة"

#: svxitems.src#RID_ATTR_PARA_ADJUST.string.text
msgid "Alignment"
msgstr "المحاذاة"

#: svxitems.src#RID_ATTR_PARA_LINESPACE.string.text
msgid "Line spacing"
msgstr "تباعد الأسطر"

#: svxitems.src#RID_ATTR_PARA_PAGEBREAK.string.text
msgid "Page Break"
msgstr "فاصل صفحات"

#: svxitems.src#RID_ATTR_PARA_HYPHENZONE.string.text svxitems.src#RID_SVXITEMS_HYPHEN_TRUE.string.text
msgid "Hyphenation"
msgstr "فصل المقاطع"

#: svxitems.src#RID_ATTR_PARA_SPLIT.string.text
msgid "Do not split paragraph"
msgstr "عدم تقسيم الفقرة"

#: svxitems.src#RID_ATTR_PARA_WIDOWS.string.text
msgid "Orphans"
msgstr "الأسطر الوحيدة"

#: svxitems.src#RID_ATTR_PARA_ORPHANS.string.text
msgid "Widows"
msgstr "الأسطر الناقصة"

#: svxitems.src#RID_ATTR_PARA_ULSPACE.string.text svxitems.src#RID_ATTR_ULSPACE.string.text
msgid "Spacing"
msgstr "التباعد"

#: svxitems.src#RID_ATTR_PARA_LRSPACE.string.text svxitems.src#RID_ATTR_LRSPACE.string.text
msgid "Indent"
msgstr "الإزاحة"

#: svxitems.src#RID_ATTR_PAGE.string.text
msgid "Page"
msgstr "صفحة"

#: svxitems.src#RID_ATTR_PARA_KEEP.string.text svxitems.src#RID_SVXITEMS_FMTKEEP_TRUE.string.text
msgid "Keep with next paragraph"
msgstr "ترابط الفقرات"

#: svxitems.src#RID_ATTR_PARA_REGISTER.string.text
#, fuzzy
msgid "Register-true"
msgstr "الانتظام"

#: svxitems.src#RID_ATTR_BRUSH_CHAR.string.text svxitems.src#RID_SVXITEMS_BRUSH_CHAR.string.text
msgid "Character background"
msgstr "خلفية الخط"

#: svxitems.src#RID_ATTR_CHAR_CJK_FONT.string.text
msgid "Asian font"
msgstr "خط آسيوي"

#: svxitems.src#RID_ATTR_CHAR_CJK_FONTHEIGHT.string.text
msgid "Size of Asian font"
msgstr "حجم الخط الآسيوي"

#: svxitems.src#RID_ATTR_CHAR_CJK_LANGUAGE.string.text
msgid "Language of Asian fonts"
msgstr "لغة الخطوط الآسيوية"

#: svxitems.src#RID_ATTR_CHAR_CJK_POSTURE.string.text
msgid "Posture of Asian fonts"
msgstr "وضعية الخطوط الآسيوية"

#: svxitems.src#RID_ATTR_CHAR_CJK_WEIGHT.string.text
msgid "Weight of Asian fonts"
msgstr "عرض الخطوط الآسيوية"

#: svxitems.src#RID_ATTR_CHAR_CTL_FONT.string.text
msgid "CTL"
msgstr "CTL"

#: svxitems.src#RID_ATTR_CHAR_CTL_FONTHEIGHT.string.text
msgid "Size of complex scripts"
msgstr "حجم النصوص المركّبة"

#: svxitems.src#RID_ATTR_CHAR_CTL_LANGUAGE.string.text
msgid "Language of complex scripts"
msgstr "لغة النصوص المركّبة"

#: svxitems.src#RID_ATTR_CHAR_CTL_POSTURE.string.text
msgid "Posture of complex scripts"
msgstr "وضعية النصوص المركّبة"

#: svxitems.src#RID_ATTR_CHAR_CTL_WEIGHT.string.text
msgid "Weight of complex scripts"
msgstr "عرض النصوص المركّبة"

#: svxitems.src#RID_ATTR_CHAR_TWO_LINES.string.text svxitems.src#RID_SVXITEMS_TWOLINES.string.text
msgid "Double-lined"
msgstr "سطرين"

#: svxitems.src#RID_ATTR_CHAR_EMPHASISMARK.string.text
msgid "Emphasis mark"
msgstr "علامة تأكيد"

#: svxitems.src#RID_ATTR_PARA_SCRIPTSPACE.string.text
msgid "Text spacing"
msgstr "تباعد النص"

#: svxitems.src#RID_ATTR_PARA_HANGPUNCTUATION.string.text
msgid "Hanging punctuation"
msgstr "علامات التنقيط المعلقة"

#: svxitems.src#RID_ATTR_PARA_FORBIDDEN_RULES.string.text
msgid "Forbidden characters"
msgstr "الأحرف الممنوعة"

#: svxitems.src#RID_ATTR_CHAR_ROTATED.string.text
msgid "Rotation"
msgstr "الاستدارة"

#: svxitems.src#RID_ATTR_CHAR_RELIEF.string.text svxitems.src#RID_SVXITEMS_RELIEF_EMBOSSED.string.text
#, fuzzy
msgid "Relief"
msgstr "نقش"

#: svxitems.src#RID_PARA_VERTALIGN.string.text
msgid "Vertical text alignment"
msgstr "محاذاة النص العمودية"

#: svxitems.src#RID_SINGLE_LINE0.string.text
msgid "Single, fine lines"
msgstr "مفرد، خطوط دقيقة"

#: svxitems.src#RID_SINGLE_LINE1.string.text
msgid "Single, thin"
msgstr "مفرد، رفيع"

#: svxitems.src#RID_SINGLE_LINE2.string.text
msgid "Single, thick"
msgstr "مفرد، ثخين"

#: svxitems.src#RID_SINGLE_LINE3.string.text
msgid "Single, very thick"
msgstr "مفرد، ثخين جداً"

#: svxitems.src#RID_SINGLE_LINE4.string.text
msgid "Single, bold"
msgstr "مفرد، عريض"

#: svxitems.src#RID_DOUBLE_LINE0.string.text
msgid "Double, fine lines, spacing: small"
msgstr "مزدوج، خط دقيق، التباعد: صغير"

#: svxitems.src#RID_DOUBLE_LINE1.string.text
msgid "Double, fine line, spacing: large"
msgstr "مزدوج، خط دقيق، التباعد: كبير"

#: svxitems.src#RID_DOUBLE_LINE2.string.text
msgid "Double, thin, spacing: small"
msgstr "مزدوج، رفيع، التباعد: صغير"

#: svxitems.src#RID_DOUBLE_LINE3.string.text
msgid "Double, thick, spacing: large"
msgstr "مزدوج، ثخين، التباعد: كبير"

#: svxitems.src#RID_DOUBLE_LINE4.string.text
msgid "Double, inside: fine lines, outside: thin, spacing: large"
msgstr "مزدوج، الداخل: خط دقيق، الخارج: رفيع، التباعد: كبير"

#: svxitems.src#RID_DOUBLE_LINE5.string.text
msgid "Double, inside: fine lines, outside: thick, spacing: large"
msgstr "مزدوج، الداخل: خط دقيق، الخارج: ثخين، التباعد: كبير"

#: svxitems.src#RID_DOUBLE_LINE6.string.text
msgid "Double, inside: fine lines, outside: very thick, spacing: large"
msgstr "مزدوج، الداخل: خط دقيق، الخارج: ثخين جداً، التباعد: كبير"

#: svxitems.src#RID_DOUBLE_LINE7.string.text
msgid "Double, inside: thin, outside: thick, spacing: large"
msgstr "مزدوج، الداخل: رفيع، الخارج: ثخين، التباعد: كبير"

#: svxitems.src#RID_DOUBLE_LINE8.string.text
msgid "Double, inside: thick, outside: thin, spacing: small"
msgstr "مزدوج، الداخل: ثخين، الخارج: رفيع، التباعد: صغير"

#: svxitems.src#RID_DOUBLE_LINE9.string.text
msgid "Double, inside: thick, outside: very thick, spacing: large"
msgstr "مزدوج، الداخل: ثخين، الخارج: ثخين جداً، التباعد: كبير"

#: svxitems.src#RID_DOUBLE_LINE10.string.text
msgid "Double, inside: very thick, outside: thick, Spacing: large"
msgstr "مزدوج، الداخل: كبير جداً، الخارج: ثخين، التباعد: كبير"

#: svxitems.src#RID_SVXITEMS_HORJUST_STANDARD.string.text
msgid "Horizontal alignment default"
msgstr "محاذاة أفقية افتراضية"

#: svxitems.src#RID_SVXITEMS_HORJUST_CENTER.string.text
msgid "Centered horizontally"
msgstr "توسيط أفقي"

#: svxitems.src#RID_SVXITEMS_HORJUST_REPEAT.string.text
msgid "Repeat alignment"
msgstr "تكرار المحاذاة"

#: svxitems.src#RID_SVXITEMS_VERJUST_STANDARD.string.text
msgid "Vertical alignment default"
msgstr "محاذاة رأسية افتراضية"

#: svxitems.src#RID_SVXITEMS_VERJUST_TOP.string.text
msgid "Align to top"
msgstr "محاذاة إلى أعلى"

#: svxitems.src#RID_SVXITEMS_VERJUST_CENTER.string.text
msgid "Centered vertically"
msgstr "توسيط عمودي"

#: svxitems.src#RID_SVXITEMS_VERJUST_BOTTOM.string.text
msgid "Align to bottom"
msgstr "محاذاة إلى أسفل"

#: svxitems.src#RID_SVXITEMS_ORI_STANDARD.string.text
msgid "Default orientation"
msgstr "اتجاه الكتابة الافتراضي"

#: svxitems.src#RID_SVXITEMS_ORI_TOPBOTTOM.string.text
msgid "From top to bottom"
msgstr "من أعلى إلى أسفل"

#: svxitems.src#RID_SVXITEMS_ORI_BOTTOMTOP.string.text
msgid "Bottom to Top"
msgstr "من أسفل إلى أعلى"

#: svxitems.src#RID_SVXITEMS_ORI_STACKED.string.text
msgid "Stacked"
msgstr "متراكمة"

#: svxitems.src#RID_SVXITEMS_SHADOWED_FALSE.string.text
msgid "Not Shadowed"
msgstr "غير مظلل"

#: svxitems.src#RID_SVXITEMS_BLINK_FALSE.string.text
msgid "Not Blinking"
msgstr "بدون وميض"

#: svxitems.src#RID_SVXITEMS_AUTOKERN_TRUE.string.text
msgid "Pair Kerning"
msgstr "تقنين أحرف زوجي"

#: svxitems.src#RID_SVXITEMS_AUTOKERN_FALSE.string.text
msgid "No pair kerning"
msgstr "بدون تقنين أحرف زوجي"

#: svxitems.src#RID_SVXITEMS_WORDLINE_FALSE.string.text
msgid "Not Words Only"
msgstr "ليس الكلمات فقطكلمة كلمة"

#: svxitems.src#RID_SVXITEMS_CONTOUR_FALSE.string.text
msgid "No Outline"
msgstr "بدون تقسيم"

#: svxitems.src#RID_SVXITEMS_PRINT_TRUE.string.text
msgid "Print"
msgstr "طباعة"

#: svxitems.src#RID_SVXITEMS_PRINT_FALSE.string.text
msgid "Don't print"
msgstr "عدم الطباعة"

#: svxitems.src#RID_SVXITEMS_OPAQUE_TRUE.string.text
msgid "Opaque"
msgstr "قاتم"

#: svxitems.src#RID_SVXITEMS_OPAQUE_FALSE.string.text
msgid "Not Opaque"
msgstr "غير قاتم"

#: svxitems.src#RID_SVXITEMS_FMTKEEP_FALSE.string.text
msgid "Don't Keep Paragraphs Together"
msgstr "عدم ترابط الفقرات"

#: svxitems.src#RID_SVXITEMS_FMTSPLIT_TRUE.string.text
msgid "Split paragraph"
msgstr "تقسيم الفقرة"

#: svxitems.src#RID_SVXITEMS_FMTSPLIT_FALSE.string.text
msgid "Don't split paragraph"
msgstr "عدم تقسيم الفقرة"

#: svxitems.src#RID_SVXITEMS_PROT_CONTENT_TRUE.string.text
msgid "Contents protected"
msgstr "المحتويات محمية"

#: svxitems.src#RID_SVXITEMS_PROT_CONTENT_FALSE.string.text
msgid "Contents not protected"
msgstr "المحتويات غير محمية"

#: svxitems.src#RID_SVXITEMS_PROT_SIZE_TRUE.string.text
msgid "Size protected"
msgstr "الحجم محمي"

#: svxitems.src#RID_SVXITEMS_PROT_SIZE_FALSE.string.text
msgid "Size not protected"
msgstr "الحجم غير محمي"

#: svxitems.src#RID_SVXITEMS_PROT_POS_TRUE.string.text
msgid "Position protected"
msgstr "الموضع محمي"

#: svxitems.src#RID_SVXITEMS_PROT_POS_FALSE.string.text
msgid "Position not protected"
msgstr "الموضع غير محمي"

#: svxitems.src#RID_SVXITEMS_TRANSPARENT_TRUE.string.text
msgid "Transparent"
msgstr "شفاف"

#: svxitems.src#RID_SVXITEMS_TRANSPARENT_FALSE.string.text
msgid "Not Transparent"
msgstr "غير شفاف"

#: svxitems.src#RID_SVXITEMS_BOXINF_TABLE_TRUE.string.text
msgid "Table"
msgstr "الجدول"

#: svxitems.src#RID_SVXITEMS_BOXINF_TABLE_FALSE.string.text
msgid "Not Table"
msgstr "بدون جدول"

#: svxitems.src#RID_SVXITEMS_BOXINF_DIST_TRUE.string.text
msgid "Spacing enabled"
msgstr "التباعد ممكّن"

#: svxitems.src#RID_SVXITEMS_BOXINF_DIST_FALSE.string.text
msgid "Spacing disabled"
msgstr "التباعد معطّل"

#: svxitems.src#RID_SVXITEMS_BOXINF_MDIST_TRUE.string.text
msgid "Keep spacing interval"
msgstr "الإبقاء على الحد الأدنى للتباعد"

#: svxitems.src#RID_SVXITEMS_BOXINF_MDIST_FALSE.string.text
msgid "Allowed to fall short of spacing interval"
msgstr "السماح بعدم الإبقاء على الحد الأدنى للتباعد"

#: svxitems.src#RID_SVXITEMS_HYPHEN_FALSE.string.text
msgid "No hyphenation"
msgstr "بدون فصل المقاطع"

#: svxitems.src#RID_SVXITEMS_PAGE_END_TRUE.string.text
msgid "Page End"
msgstr "نهاية الصفحة"

#: svxitems.src#RID_SVXITEMS_PAGE_END_FALSE.string.text
msgid "No Page End"
msgstr "بدون نهاية صفحة"

#: svxitems.src#RID_SVXITEMS_MARGIN_LEFT.string.text
msgid "Left margin: "
msgstr "الهامش الأيسر: "

#: svxitems.src#RID_SVXITEMS_MARGIN_TOP.string.text
msgid "Top margin: "
msgstr "الهامش العلوي: "

#: svxitems.src#RID_SVXITEMS_MARGIN_RIGHT.string.text
msgid "Right margin: "
msgstr "الهامش الأيمن: "

#: svxitems.src#RID_SVXITEMS_MARGIN_BOTTOM.string.text
msgid "Bottom margin: "
msgstr "الهامش السفلي: "

#: svxitems.src#RID_SVXITEMS_SIZE_WIDTH.string.text
msgid "Width: "
msgstr "العرض: "

#: svxitems.src#RID_SVXITEMS_SIZE_HEIGHT.string.text
msgid "Height: "
msgstr "الارتفاع: "

#: svxitems.src#RID_SVXITEMS_LRSPACE_LEFT.string.text
msgid "Indent left "
msgstr "إزاحة يسرى "

#: svxitems.src#RID_SVXITEMS_LRSPACE_FLINE.string.text
msgid "First Line "
msgstr "السطر الأول "

#: svxitems.src#RID_SVXITEMS_LRSPACE_RIGHT.string.text
msgid "Indent right "
msgstr "إزاحة يمنى "

#: svxitems.src#RID_SVXITEMS_SHADOW_COMPLETE.string.text
msgid "Shadow: "
msgstr "الظل: "

#: svxitems.src#RID_SVXITEMS_BORDER_COMPLETE.string.text
msgid "Borders "
msgstr "الحدود "

#: svxitems.src#RID_SVXITEMS_BORDER_NONE.string.text
msgid "No border"
msgstr "بدون حدود"

#: svxitems.src#RID_SVXITEMS_BORDER_TOP.string.text
msgid "top "
msgstr "أعلى "

#: svxitems.src#RID_SVXITEMS_BORDER_BOTTOM.string.text
msgid "bottom "
msgstr "أسفل "

#: svxitems.src#RID_SVXITEMS_BORDER_LEFT.string.text
msgid "left "
msgstr "يسار "

#: svxitems.src#RID_SVXITEMS_BORDER_RIGHT.string.text
msgid "right "
msgstr "يمين "

#: svxitems.src#RID_SVXITEMS_BORDER_DISTANCE.string.text
msgid "Spacing "
msgstr "تباعد "

#: svxitems.src#RID_SVXITEMS_ULSPACE_UPPER.string.text
msgid "From top "
msgstr "من أعلى "

#: svxitems.src#RID_SVXITEMS_ULSPACE_LOWER.string.text
msgid "From bottom "
msgstr "من أسفل "

#: svxitems.src#RID_SVXITEMS_PAGE_COMPLETE.string.text
msgid "Page Description: "
msgstr "وصف الصفحة: "

#: svxitems.src#RID_SVXITEMS_PAGE_NUM_CHR_UPPER.string.text
msgid "Capitals"
msgstr "أحرف كبيرة"

#: svxitems.src#RID_SVXITEMS_PAGE_NUM_ROM_UPPER.string.text
msgid "Uppercase Roman"
msgstr "أحرف استهلالية رومانية"

#: svxitems.src#RID_SVXITEMS_PAGE_NUM_ROM_LOWER.string.text
msgid "Lowercase Roman"
msgstr "أحرف صغيرة رومانية"

#: svxitems.src#RID_SVXITEMS_PAGE_NUM_ARABIC.string.text
msgid "Arabic"
msgstr "العربية"

#: svxitems.src#RID_SVXITEMS_PAGE_LAND_TRUE.string.text
msgid "Landscape"
msgstr "بالعرض"

#: svxitems.src#RID_SVXITEMS_PAGE_LAND_FALSE.string.text
msgid "Portrait"
msgstr "بالطول"

#: svxitems.src#RID_SVXITEMS_PAGE_USAGE_ALL.string.text
msgid "All"
msgstr "الكل"

#: svxitems.src#RID_SVXITEMS_PAGE_USAGE_MIRROR.string.text
msgid "Mirrored"
msgstr "منعكس"

#: svxitems.src#RID_SVXITEMS_LINES.string.text
msgid "Lines"
msgstr "خطوط"

#: svxitems.src#RID_SVXITEMS_WIDOWS_COMPLETE.string.text
msgid "Orphan control "
msgstr "التحكم بالأسطر الوحيدة "

#: svxitems.src#RID_SVXITEMS_ORPHANS_COMPLETE.string.text
msgid "Widow control "
msgstr "التحكم بالأسطر الناقصة "

#: svxitems.src#RID_SVXITEMS_HYPHEN_MINLEAD.string.text
msgid "Characters at end of line"
msgstr "أحرف عند نهاية السطر"

#: svxitems.src#RID_SVXITEMS_HYPHEN_MINTRAIL.string.text
msgid "Characters at beginning of line"
msgstr "أحرف عند بداية السطر"

#: svxitems.src#RID_SVXITEMS_HYPHEN_MAX.string.text
msgid "Hyphens"
msgstr "مناطق فصل"

#: svxitems.src#RID_SVXITEMS_PAGEMODEL_COMPLETE.string.text
msgid "Page Style: "
msgstr "نمط الصفحة: "

#: svxitems.src#RID_SVXITEMS_KERNING_COMPLETE.string.text
msgid "Kerning "
msgstr "تقنين الأحرف "

#: svxitems.src#RID_SVXITEMS_KERNING_EXPANDED.string.text
msgid "locked "
msgstr "مُقغل"

#: svxitems.src#RID_SVXITEMS_KERNING_CONDENSED.string.text
msgid "Condensed "
msgstr "مكثف "

#: svxitems.src#RID_SVXITEMS_AUTHOR_COMPLETE.string.text
msgid "Author: "
msgstr "المؤلف: "

#: svxitems.src#RID_SVXITEMS_DATE_COMPLETE.string.text
msgid "Date: "
msgstr "التاريخ: "

#: svxitems.src#RID_SVXITEMS_TEXT_COMPLETE.string.text
msgid "Text: "
msgstr "النص: "

#: svxitems.src#RID_SVXITEMS_BACKGROUND_COLOR.string.text
msgid "Background color: "
msgstr "لون الخلفية: "

#: svxitems.src#RID_SVXITEMS_PATTERN_COLOR.string.text
msgid "Pattern color: "
msgstr "لون النقش: "

#: svxitems.src#RID_SVXITEMS_GRAPHIC.string.text
msgid "Graphic"
msgstr "رسم"

#: svxitems.src#RID_SVXITEMS_EMPHASIS_NONE_STYLE.string.text
msgid "none"
msgstr "بدون"

#: svxitems.src#RID_SVXITEMS_EMPHASIS_DOT_STYLE.string.text
msgid "Dots "
msgstr "نقاط"

#: svxitems.src#RID_SVXITEMS_EMPHASIS_CIRCLE_STYLE.string.text
msgid "Circle "
msgstr "دائرة"

#: svxitems.src#RID_SVXITEMS_EMPHASIS_DISC_STYLE.string.text
msgid "Filled circle "
msgstr "دائرة ممتلئة"

#: svxitems.src#RID_SVXITEMS_EMPHASIS_ACCENT_STYLE.string.text
msgid "Accent "
msgstr "علامة نطقية "

#: svxitems.src#RID_SVXITEMS_EMPHASIS_ABOVE_POS.string.text
msgid "Above"
msgstr "أعلى"

#: svxitems.src#RID_SVXITEMS_EMPHASIS_BELOW_POS.string.text
msgid "Below"
msgstr "أسفل"

#: svxitems.src#RID_SVXITEMS_TWOLINES_OFF.string.text
msgid "Double-lined off"
msgstr "عدم الكتابة في سطرين"

#: svxitems.src#RID_SVXITEMS_SCRPTSPC_OFF.string.text svxitems.src#RID_SVXITEMS_SCRPTSPC_ON.string.text
msgid "No automatic character spacing"
msgstr "بدون تباعد تلقائي للأحرف"

#: svxitems.src#RID_SVXITEMS_HNGPNCT_OFF.string.text
msgid "No hanging punctuation at line end"
msgstr "بدون علامات تنقيط معلقة عند نهاية السطر"

#: svxitems.src#RID_SVXITEMS_HNGPNCT_ON.string.text
msgid "Hanging punctuation at line end"
msgstr "علامات تنقيط معلقة عند نهاية السطر"

#: svxitems.src#RID_SVXITEMS_FORBIDDEN_RULE_OFF.string.text
msgid "Apply list of forbidden characters to beginning and end of lines"
msgstr "تطبيق الأحرف الممنوعة على بداية الأسطر ونهايتها"

#: svxitems.src#RID_SVXITEMS_FORBIDDEN_RULE_ON.string.text
msgid "Don't apply list of forbidden characters to beginning and end of lines"
msgstr "عدم تطبيق الأحرف الممنوعة على بداية الأسطر ونهايتها"

#: svxitems.src#RID_SVXITEMS_CHARROTATE_OFF.string.text
msgid "No rotated characters"
msgstr "بدون استدارة أحرف"

#: svxitems.src#RID_SVXITEMS_CHARROTATE.string.text
msgid "Character rotated by $(ARG1)°"
msgstr "استدارة الأحرف بمقدار $(ARG1)°"

#: svxitems.src#RID_SVXITEMS_CHARROTATE_FITLINE.string.text
msgid "Fit to line"
msgstr "ملاءمة للسطر"

#: svxitems.src#RID_SVXITEMS_CHARSCALE.string.text
msgid "Characters scaled $(ARG1)%"
msgstr "تحجيم الأحرف $(ARG1)%"

#: svxitems.src#RID_SVXITEMS_CHARSCALE_OFF.string.text
msgid "No scaled characters"
msgstr "بدون تحجيم الأحرف"

#: svxitems.src#RID_SVXITEMS_RELIEF_NONE.string.text
#, fuzzy
msgid "No relief"
msgstr "بدون نقش"

#: svxitems.src#RID_SVXITEMS_RELIEF_ENGRAVED.string.text
msgid "Engraved"
msgstr "محفور"

#: svxitems.src#RID_SVXITEMS_PARAVERTALIGN_AUTO.string.text
msgid "Automatic text alignment"
msgstr "محاذاة تلقائية للنص"

#: svxitems.src#RID_SVXITEMS_PARAVERTALIGN_BASELINE.string.text
msgid "Text aligned to base line"
msgstr "محاذاة النص لخط الأساس"

#: svxitems.src#RID_SVXITEMS_PARAVERTALIGN_TOP.string.text
msgid "Text aligned top"
msgstr "محاذاة النص لأعلى"

#: svxitems.src#RID_SVXITEMS_PARAVERTALIGN_CENTER.string.text
msgid "Text aligned middle"
msgstr "محاذاة النص للوسط"

#: svxitems.src#RID_SVXITEMS_PARAVERTALIGN_BOTTOM.string.text
msgid "Text aligned bottom"
msgstr "محاذاة النص لأسفل"

#: svxitems.src#RID_SVXITEMS_FRMDIR_HORI_LEFT_TOP.string.text
msgid "Text direction left-to-right (horizontal)"
msgstr "اتجاه النص من اليسار إلى اليمين (أفقياً)"

#: svxitems.src#RID_SVXITEMS_FRMDIR_HORI_RIGHT_TOP.string.text
msgid "Text direction right-to-left (horizontal)"
msgstr "اتجاه النص من اليمين إلى اليسار (أفقياً)"

#: svxitems.src#RID_SVXITEMS_FRMDIR_VERT_TOP_RIGHT.string.text
msgid "Text direction right-to-left (vertical)"
msgstr "اتجاه النص من اليمين إلى اليسار (عمودياً)"

#: svxitems.src#RID_SVXITEMS_FRMDIR_VERT_TOP_LEFT.string.text
msgid "Text direction left-to-right (vertical)"
msgstr "اتجاه النص من اليسار إلى اليمين (عمودياً)"

#: svxitems.src#RID_SVXITEMS_FRMDIR_ENVIRONMENT.string.text
msgid "Use superordinate object text direction setting"
msgstr "استخدام إعدادات اتجاه النص الكائن"

#: svxitems.src#RID_SVXITEMS_PARASNAPTOGRID_ON.string.text
msgid "Paragraph snaps to text grid (if active)"
msgstr "انطباق الفقرة على شبكة النص (إن كانت فعالة)"

#: svxitems.src#RID_SVXITEMS_PARASNAPTOGRID_OFF.string.text
msgid "Paragraph does not snap to text grid"
msgstr "عدم انطباق الفقرة على شبكة النص"

#: svxitems.src#RID_SVXITEMS_CHARHIDDEN_FALSE.string.text
msgid "Not hidden"
msgstr "غير مخفي"

#: svxitems.src#RID_SVXITEMS_CHARHIDDEN_TRUE.string.text
msgid "Hidden"
msgstr "مخفي"

