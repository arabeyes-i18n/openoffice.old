# translation of textconversiondlgs.po to Arabic
# extracted from svx/source/unodialogs/textconversiondlgs.oo
# Ossama M. Khayat <okhayat@yahoo.com>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: textconversiondlgs\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2004-12-30 02:07+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.CB_REVERSE.checkbox.text
msgid "Reverse mapping"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.FT_TERM.fixedtext.text
msgid "Term"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.FT_MAPPING.fixedtext.text
msgid "Mapping"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.FT_PROPERTY.fixedtext.text
#, fuzzy
msgid "Property"
msgstr "خاصيّة"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.1.stringlist.text
#, fuzzy
msgid "Other"
msgstr "غير ذلك"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.2.stringlist.text
msgid "Foreign"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.3.stringlist.text
#, fuzzy
msgid "First name"
msgstr "الاسم"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.4.stringlist.text
#, fuzzy
msgid "Last name"
msgstr "اسم العائلة"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.5.stringlist.text
#, fuzzy
msgid "Title"
msgstr "العنوان"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.6.stringlist.text
#, fuzzy
msgid "Status"
msgstr "الحالة"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.7.stringlist.text
msgid "Place name"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.8.stringlist.text
#, fuzzy
msgid "Business"
msgstr "عمل"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.9.stringlist.text
msgid "Adjective"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.10.stringlist.text
msgid "Idiom"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.11.stringlist.text
msgid "Abbreviation"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.12.stringlist.text
msgid "Numerical"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.13.stringlist.text
msgid "Noun"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.14.stringlist.text
msgid "Verb"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.LB_PROPERTY.15.stringlist.text
msgid "Brand name"
msgstr ""

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.PB_ADD.pushbutton.text
#, fuzzy
msgid "~Add"
msgstr "إضافة"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.PB_MODIFY.pushbutton.text
#, fuzzy
msgid "~Modify"
msgstr "تعديل"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.PB_DELETE.pushbutton.text
#, fuzzy
msgid "~Delete"
msgstr "حذف"

#: chinese_dictionarydialog.src#DLG_CHINESEDICTIONARY.modaldialog.text
msgid "Edit Dictionary"
msgstr ""

#: chinese_translationdialog.src#DLG_CHINESETRANSLATION.FL_DIRECTION.fixedline.text
msgid "Translation direction"
msgstr ""

#: chinese_translationdialog.src#DLG_CHINESETRANSLATION.CB_USE_VARIANTS.checkbox.text
msgid "~Use Taiwan, Hong Kong SAR, and Macao SAR character variants"
msgstr ""

#: chinese_translationdialog.src#DLG_CHINESETRANSLATION.FL_COMMONTERMS.fixedline.text
msgid "Common terms"
msgstr ""

#: chinese_translationdialog.src#DLG_CHINESETRANSLATION.CB_TRANSLATE_COMMONTERMS.checkbox.text
msgid "Translate ~common terms"
msgstr ""

#: chinese_translationdialog.src#DLG_CHINESETRANSLATION.PB_EDITTERMS.pushbutton.text
msgid "~Edit Terms..."
msgstr ""

#: chinese_translationdialog.src#DLG_CHINESETRANSLATION.modaldialog.text
msgid "Chinese Translation"
msgstr ""

#: chinese_direction_tmpl.hrc#DIRECTION_RADIOBUTTONS__D_XPOS__D_YPOS__D_FULLWIDTH__.RB_TO_SIMPLIFIED.radiobutton.text
msgid "~Traditional Chinese to simplified Chinese"
msgstr ""

#: chinese_direction_tmpl.hrc#DIRECTION_RADIOBUTTONS__D_XPOS__D_YPOS__D_FULLWIDTH__.RB_TO_TRADITIONAL.radiobutton.text
msgid "~Simplified Chinese to traditional Chinese"
msgstr ""
