# translation of basicide.po to Arabic
# extracted from basctl/source/basicide.oo
# Ossama M. Khayat <okhayat@yahoo.com>, 2004, 2005.
msgid ""
msgstr ""
"Project-Id-Version: basicide\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2005-03-26 23:34+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#: basidesh.src#RID_STR_FILTER_ALLFILES.string.text
msgid "<All>"
msgstr "<الكل>"

#: basidesh.src#RID_STR_NOMODULE.string.text
msgid "< No Module >"
msgstr "< بدون وحدة نمطية >"

#: basidesh.src#RID_STR_WRONGPASSWORD.string.text
msgid "Incorrect Password"
msgstr "كلمة مرور غير صحيحة"

#: basidesh.src#RID_STR_OPEN.string.text
msgid "Load"
msgstr "تحميل"

#: basidesh.src#RID_STR_SAVE.string.text
msgid "Save"
msgstr "حفظ"

#: basidesh.src#RID_STR_SOURCETOBIG.string.text
msgid "The source text is too large and can be neither compiled nor saved.\\nDelete some of the comments or transfer some methods into another module."
msgstr ""
"النص المصدري ضخم جداً ولا يمكن ترجمته أو حفظه.\n"
"احذف بعض الملاحظات أو انقل بعض الأساليب إلى وحدة نمطية أخرى."

#: basidesh.src#RID_STR_ERROROPENSTORAGE.string.text
msgid "Error opening file"
msgstr "خطأ أثناء فتح الملف"

#: basidesh.src#RID_STR_ERROROPENLIB.string.text
msgid "Error loading library"
msgstr "خطأ أثناء تحميل المكتبة"

#: basidesh.src#RID_STR_NOLIBINSTORAGE.string.text
msgid "The file does not contain any BASIC libraries"
msgstr ".BASIC لا يحتوي الملف على أية مكتبات"

#: basidesh.src#RID_STR_BADSBXNAME.string.text
msgid "Invalid Name"
msgstr "اسم غير صالح"

#: basidesh.src#RID_STR_LIBNAMETOLONG.string.text
msgid "A library name can have up to 30 characters."
msgstr "يجب ألا يزيد اسم المكتبة عن 30 حرفاً."

#: basidesh.src#RID_STR_ERRORCHOOSEMACRO.string.text
msgid "Macros from other documents are not accessible."
msgstr "مركبات المستندات الأخرى غير قابلة للنفاذ."

#: basidesh.src#RID_STR_LIBISREADONLY.string.text
msgid "This library is read-only."
msgstr "هذه المكتبة للقراءة فقط."

#: basidesh.src#RID_STR_REPLACELIB.string.text
msgid "'XX' cannot be replaced."
msgstr "لا يمكن استبدال 'XX'."

#: basidesh.src#RID_STR_IMPORTNOTPOSSIBLE.string.text
msgid "'XX' cannot be added."
msgstr "لا يمكن إضافة 'XX'."

#: basidesh.src#RID_STR_NOIMPORT.string.text
msgid "'XX' was not added."
msgstr "لم تتم إضافة 'XX'."

#: basidesh.src#RID_STR_ENTERPASSWORD.string.text
msgid "Enter password for 'XX'"
msgstr "أدخل كلمة المرور الخاصة بـ'XX'"

#: basidesh.src#RID_STR_SBXNAMEALLREADYUSED.string.text
msgid "Name already exists"
msgstr "هذا الاسم موجود مسبقاً"

#: basidesh.src#RID_STR_SIGNED.string.text
msgid "(Signed)"
msgstr "(مُوقّع)"

#: basidesh.src#RID_STR_SBXNAMEALLREADYUSED2.string.text
msgid "Object with same name already exists"
msgstr "يوجد مسبقاً كائن له نفس الاسم"

#: basidesh.src#RID_STR_FILEEXISTS.string.text
msgid "The 'XX' file already exists"
msgstr "موجود مسبقاً 'XX' الملف"

#: basidesh.src#RID_STR_CANNOTRUNMACRO.string.text
msgid "For security reasons, you cannot run this macro.\\n\\nFor more information, check the security settings."
msgstr ""
"لأسباب أمنية، لا يمكنك تشغيل وحدة ماكرو هذه.\n"
"للمزيد من المعلومات، تحقق من إعدادات الأمن."

#: basidesh.src#RID_STR_COMPILEERROR.string.text
msgid "Compile Error: "
msgstr "خطأ في الترجمة: "

#: basidesh.src#RID_STR_RUNTIMEERROR.string.text
msgid "Runtime Error: #"
msgstr "خطأ في وقت التشغيل: #"

#: basidesh.src#RID_STR_SEARCHNOTFOUND.string.text
msgid "Search key not found"
msgstr "تعذر العثور على كلمة البحث"

#: basidesh.src#RID_STR_SEARCHFROMSTART.string.text
msgid "Search to last module complete. Continue at first module?"
msgstr "تم البحث حتى الوحدة النمطية الأخيرة. هل تريد مواصلة البحث عند الوحدة النمطية الأولى؟"

#: basidesh.src#RID_STR_SEARCHREPLACES.string.text
msgid "Search key replaced XX times"
msgstr "تم استبدال بيانات البحث XX مرة"

#: basidesh.src#RID_STR_COULDNTREAD.string.text
msgid "The file could not be read"
msgstr "تعذرت قراءة الملف"

#: basidesh.src#RID_STR_COULDNTWRITE.string.text
msgid "The file could not be saved"
msgstr "تعذر حفظ الملف"

#: basidesh.src#RID_STR_CANNOTCHANGENAMESTDLIB.string.text
msgid "The name of the default library cannot be changed."
msgstr "لا يمكن تغيير اسم المكتبة الافتراضية."

#: basidesh.src#RID_STR_CANNOTCHANGENAMEREFLIB.string.text
msgid "The name of a referenced library cannot be changed."
msgstr "لا يمكن تغيير اسم مكتبة مرجعية."

#: basidesh.src#RID_STR_CANNOTUNLOADSTDLIB.string.text
msgid "The default library cannot be deactivated"
msgstr "لا يمكن إلغاء تنشيط المكتبة الافتراضية"

#: basidesh.src#RID_STR_GENERATESOURCE.string.text
msgid "Generating source"
msgstr "إنشاء نص مصدر"

#: basidesh.src#RID_STR_FILENAME.string.text
msgid "File name:"
msgstr "اسم الملف:"

#: basidesh.src#RID_STR_APPENDLIBS.string.text
msgid "Append Libraries"
msgstr "إلحاق مكتبات"

#: basidesh.src#RID_STR_QUERYDELMACRO.string.text
msgid "Do you want to delete the macro XX ?"
msgstr "هل تريد حذف الماكرو XX؟"

#: basidesh.src#RID_STR_QUERYDELDIALOG.string.text
msgid "Do you want to delete the XX dialog?"
msgstr "هل تريد حذف مربع الحوار XX؟"

#: basidesh.src#RID_STR_QUERYDELLIB.string.text
msgid "Do you want to delete the XX library?"
msgstr "هل تريد حذف المكتبة XX؟"

#: basidesh.src#RID_STR_QUERYDELLIBREF.string.text
msgid "Do you want to delete the reference to the XX library?"
msgstr "هل تريد حذف المرجع إلى المكتبة XX؟"

#: basidesh.src#RID_STR_QUERYDELMODULE.string.text
msgid "Do you want to delete the XX module?"
msgstr "هل تريد حذف الوحدة النمطية XX؟"

#: basidesh.src#RID_STR_OBJNOTFOUND.string.text
msgid "Object or method not found"
msgstr "تعذر العثور على الكائن أو الأسلوب"

#: basidesh.src#RID_STR_BASIC.string.text
msgid "BASIC"
msgstr "بيسك"

#: basidesh.src#RID_STR_LINE.string.text
msgid "Ln"
msgstr "سطر"

#: basidesh.src#RID_STR_COLUMN.string.text
msgid "Col"
msgstr "عمود"

#: basidesh.src#RID_STR_DOC.string.text
msgid "Document"
msgstr "المستند"

#: basidesh.src#RID_BASICIDE_OBJECTBAR.string.text
msgid "Macro Bar"
msgstr "شريط ماكرو"

#: basidesh.src#RID_STR_CANNOTCLOSE.string.text
msgid "The window cannot be closed while BASIC is running."
msgstr "لا يمكن إغلاق النافذة، أثناء تشغيل برنامج بيسك."

#: basidesh.src#RID_STR_REPLACESTDLIB.string.text
msgid "The default library cannot be replaced."
msgstr "لا يمكن استبدال المكتبة الافتراضية."

#: basidesh.src#RID_STR_REFNOTPOSSIBLE.string.text
msgid "Reference to 'XX' not possible."
msgstr "المرجع إلى 'XX' غير ممكن."

#: basidesh.src#RID_STR_WATCHNAME.string.text
msgid "Watch"
msgstr "مراقب"

#: basidesh.src#RID_STR_WATCHVARIABLE.string.text
msgid "Variable"
msgstr "متغير"

#: basidesh.src#RID_STR_WATCHVALUE.string.text
msgid "Value"
msgstr "قيمة"

#: basidesh.src#RID_STR_WATCHTYPE.string.text
msgid "Type"
msgstr "نوع"

#: basidesh.src#RID_STR_STACKNAME.string.text
msgid "Call Stack"
msgstr "استدعاء المكَّدس"

#: basidesh.src#RID_STR_INITIDE.string.text
msgid "BASIC Initialization"
msgstr "ابتداء BASIC"

#: basidesh.src#RID_STR_STDMODULENAME.string.text
msgid "Module"
msgstr "وحدة نمطية"

#: basidesh.src#RID_STR_STDDIALOGNAME.string.text moduldlg.src#RID_TP_DLGS.RID_STR_LIB.fixedtext.text
msgid "Dialog"
msgstr "مربع حوار"

#: basidesh.src#RID_STR_STDLIBNAME.string.text
msgid "Library"
msgstr "مكتبة"

#: basidesh.src#RID_STR_NEWLIB.string.text
msgid "New Library"
msgstr "مكتبة جديدة"

#: basidesh.src#RID_STR_NEWMOD.string.text
msgid "New Module"
msgstr "وحدة نمطية جديدة"

#: basidesh.src#RID_STR_NEWDLG.string.text
msgid "New Dialog"
msgstr "مربع حوار جديد"

#: basidesh.src#RID_STR_ALL.string.text
msgid "All"
msgstr "الكل"

#: basidesh.src#RID_STR_PAGE.string.text
msgid "Page"
msgstr "صفحة"

#: basidesh.src#RID_STR_MACRONAMEREQ.string.text
msgid "A name must be entered."
msgstr "يجب إدخال اسم."

#: basidesh.src#RID_STR_WILLSTOPPRG.string.text
msgid "You will have to restart the program after this edit.\\nContinue?"
msgstr ""
"ستحتاج إلى إعادة تشغيل البرنامج بعد هذا التعديل.\n"
"هل أكمل؟"

#: basidesh.src#RID_STR_SEARCHALLMODULES.string.text
msgid "Do you want to replace the text in all active modules?"
msgstr "هل تريد استبدال النص في كل الوحدات النمطية النشطة؟"

#: basidesh.src#RID_IMGBTN_REMOVEWATCH.imagebutton.text
msgid "-"
msgstr "-"

#: basidesh.src#RID_IMGBTN_REMOVEWATCH.imagebutton.quickhelptext
msgid "Remove Watch"
msgstr "إزالة المراقب"

#: basidesh.src#RID_STR_REMOVEWATCH.string.text
msgid "Watch:"
msgstr "المراقب: "

#: basidesh.src#RID_STR_STACK.string.text
msgid "Calls: "
msgstr "استدعاءات:"

#: basidesh.src#RID_STR_USERMACROS.string.text
msgid "My Macros"
msgstr "وحدات الماكرو خاصتي"

#: basidesh.src#RID_STR_USERDIALOGS.string.text
msgid "My Dialogs"
msgstr "مربعات الحوار خاصتي"

#: basidesh.src#RID_STR_USERMACROSDIALOGS.string.text
msgid "My Macros & Dialogs"
msgstr "وحدات الماكرو ومربعات الحوار خاصتي"

#: basidesh.src#RID_STR_SHAREMACROS.string.text
msgid "%PRODUCTNAME Macros"
msgstr "وحدات ماكرو %PRODUCTNAME"

#: basidesh.src#RID_STR_SHAREDIALOGS.string.text
msgid "%PRODUCTNAME Dialogs"
msgstr "مربعات حوار %PRODUCTNAME"

#: basidesh.src#RID_STR_SHAREMACROSDIALOGS.string.text
msgid "%PRODUCTNAME Macros & Dialogs"
msgstr "وحدات ماكرو ومربعات حوار %PRODUCTNAME"

#: basidesh.src#RID_POPUP_BRKPROPS.RID_ACTIV.menuitem.text brkdlg.src#RID_BASICIDE_BREAKPOINTDLG.RID_CHKB_ACTIVE.checkbox.text
msgid "Active"
msgstr "نشط"

#: basidesh.src#RID_POPUP_BRKPROPS.RID_BRKPROPS.menuitem.text basidesh.src#RID_POPUP_DLGED.SID_SHOW_PROPERTYBROWSER.menuitem.text
msgid "Properties..."
msgstr "خصائص..."

#: basidesh.src#RID_POPUP_BRKPROPS.menu.text tbxctl.src#RID_TOOLBOX.SID_SHOW_PROPERTYBROWSER.toolboxitem.text
msgid "Properties"
msgstr "خصائص"

#: basidesh.src#RID_POPUP_BRKDLG.RID_BRKDLG.menuitem.text
msgid "Manage Breakpoints..."
msgstr "إدارة نقاط الإيقاف..."

#: basidesh.src#RID_POPUP_BRKDLG.menu.text brkdlg.src#RID_BASICIDE_BREAKPOINTDLG.modaldialog.text
msgid "Manage Breakpoints"
msgstr "إدارة نقاط التوقف"

#: basidesh.src#RID_POPUP_TABBAR.RID_INSERT.SID_BASICIDE_NEWMODULE.menuitem.text
msgid "BASIC Module"
msgstr "وحدة بيسك نمطية"

#: basidesh.src#RID_POPUP_TABBAR.RID_INSERT.SID_BASICIDE_NEWDIALOG.menuitem.text
msgid "BASIC Dialog"
msgstr "مربع حوار بيسك"

#: basidesh.src#RID_POPUP_TABBAR.RID_INSERT.menuitem.text
msgid "Insert"
msgstr "إدراج"

#: basidesh.src#RID_POPUP_TABBAR.SID_BASICIDE_DELETECURRENT.menuitem.text brkdlg.src#RID_BASICIDE_BREAKPOINTDLG.RID_PB_DEL.pushbutton.text
msgid "Delete"
msgstr "حذف"

#: basidesh.src#RID_POPUP_TABBAR.SID_BASICIDE_RENAMECURRENT.menuitem.text
msgid "Rename"
msgstr "إعادة تسمية"

#: basidesh.src#RID_POPUP_TABBAR.SID_BASICIDE_HIDECURPAGE.menuitem.text
msgid "Hide"
msgstr "إخفاء"

#: basidesh.src#RID_POPUP_TABBAR.SID_BASICIDE_MODULEDLG.menuitem.text
msgid "Modules..."
msgstr "وحدات نمطية..."

#: basidesh.src#RID_STR_QUERYREPLACEMACRO.string.text
msgid "Do you want to overwrite the XX macro?"
msgstr "هل تريد الكتابة فوق ماكرو XX؟"

#: brkdlg.src#RID_BASICIDE_BREAKPOINTDLG.RID_PB_NEW.pushbutton.text
msgid "New"
msgstr "جديد"

#: brkdlg.src#RID_BASICIDE_BREAKPOINTDLG.RID_FT_PASS.fixedtext.text
msgid "Pass Count:"
msgstr "تعداد المرات:"

#: brkdlg.src#RID_BASICIDE_BREAKPOINTDLG.RID_FT_BRKPOINTS.fixedtext.text
msgid "Breakpoints"
msgstr "نقاط الإيقاف"

#: macrodlg.src#RID_MACROCHOOSER.RID_TXT_MACROSIN.fixedtext.text
msgid "Existing macros ~in:"
msgstr "وحدات الماكرو الحالية ~في:"

#: macrodlg.src#RID_MACROCHOOSER.RID_TXT_MACRONAME.fixedtext.text
msgid "~Macro name"
msgstr "اسم ال~ماكرو"

#: macrodlg.src#RID_MACROCHOOSER.RID_TXT_MACROFROM.fixedtext.text
msgid "Macro ~from"
msgstr "ماكرو م~ن"

#: macrodlg.src#RID_MACROCHOOSER.RID_TXT_SAVEMACRO.fixedtext.text
msgid "Save m~acro in"
msgstr "حفظ الما~كرو في"

#: macrodlg.src#RID_MACROCHOOSER.RID_TXT_DESCRIPTION.fixedtext.text
msgid "De~scription"
msgstr "ال~وصف"

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_RUN.pushbutton.text
msgid "R~un"
msgstr "~تنفيذ"

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_CLOSE.cancelbutton.text macrodlg.src#RID_STR_CLOSE.string.text moduldlg.src#RID_TP_MODULS.RID_PB_CLOSE.cancelbutton.text
#: moduldlg.src#RID_TP_DLGS.RID_PB_CLOSE.cancelbutton.text moduldlg.src#RID_TP_LIBS.RID_PB_CLOSE.cancelbutton.text
msgid "Close"
msgstr "إغلاق"

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_ASSIGN.pushbutton.text
msgid "~Assign..."
msgstr "ت~عيين..."

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_EDIT.pushbutton.text moduldlg.src#RID_TP_MODULS.RID_PB_EDIT.pushbutton.text moduldlg.src#RID_TP_DLGS.RID_PB_EDIT.pushbutton.text
#: moduldlg.src#RID_TP_LIBS.RID_PB_EDIT.pushbutton.text
msgid "~Edit"
msgstr "~تحرير"

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_NEWLIB.pushbutton.text
msgid "New ~Library"
msgstr "~مكتبة جديدة"

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_NEWMOD.pushbutton.text
msgid "New M~odule"
msgstr "وحدة ~نمطية جديدة"

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_DEL.pushbutton.text macrodlg.src#RID_STR_BTNDEL.string.text moduldlg.src#RID_TP_MODULS.RID_PB_DELETE.pushbutton.text
#: moduldlg.src#RID_TP_DLGS.RID_PB_DELETE.pushbutton.text moduldlg.src#RID_TP_LIBS.RID_PB_DELETE.pushbutton.text
msgid "~Delete"
msgstr "حذف"

#: macrodlg.src#RID_MACROCHOOSER.RID_PB_ORG.pushbutton.text
msgid "~Organizer..."
msgstr "من~ظّم..."

#: macrodlg.src#RID_MACROCHOOSER.modaldialog.text
msgid "%PRODUCTNAME Basic Macros"
msgstr "وحدات بيسك ماكرو %PRODUCTNAME"

#: macrodlg.src#RID_STR_STDMACRONAME.string.text
msgid "Macro"
msgstr "ماكرو"

#: macrodlg.src#RID_STR_BTNNEW.string.text
msgid "~New"
msgstr "~جديد"

#: macrodlg.src#RID_STR_CHOOSE.string.text
msgid "Choose"
msgstr "اختيار"

#: macrodlg.src#RID_STR_RUN.string.text
msgid "Run"
msgstr "تشغيل"

#: macrodlg.src#RID_STR_RECORD.string.text
msgid "~Save"
msgstr "~حفظ"

#: moduldlg.src#RID_TD_ORGANIZE.RID_TC_ORGANIZE.RID_TP_MOD.pageitem.text
msgid "Modules"
msgstr "وحدات نمطية"

#: moduldlg.src#RID_TD_ORGANIZE.RID_TC_ORGANIZE.RID_TP_DLG.pageitem.text
msgid "Dialogs"
msgstr "مربعات حوار"

#: moduldlg.src#RID_TD_ORGANIZE.RID_TC_ORGANIZE.RID_TP_LIB.pageitem.text
msgid "Libraries"
msgstr "مكتبات"

#: moduldlg.src#RID_TD_ORGANIZE.tabdialog.text
msgid "%PRODUCTNAME Basic Macro Organizer"
msgstr "منظم وحدات بيسك ماكرو %PRODUCTNAME"

#: moduldlg.src#RID_TP_MODULS.RID_STR_LIB.fixedtext.text
msgid "M~odule"
msgstr "~وحدة نمطية"

#: moduldlg.src#RID_TP_MODULS.RID_PB_NEWMOD.pushbutton.text moduldlg.src#RID_TP_MODULS.RID_PB_NEWDLG.pushbutton.text moduldlg.src#RID_TP_DLGS.RID_PB_NEWMOD.pushbutton.text
#: moduldlg.src#RID_TP_DLGS.RID_PB_NEWDLG.pushbutton.text moduldlg.src#RID_TP_LIBS.RID_PB_NEWLIB.pushbutton.text
msgid "~New..."
msgstr "~جديد..."

#: moduldlg.src#RID_TP_LIBS.RID_STR_BASICS.fixedtext.text
msgid "L~ocation"
msgstr "~موقع"

#: moduldlg.src#RID_TP_LIBS.RID_STR_LIB.fixedtext.text
msgid "~Library"
msgstr "~مكتبة"

#: moduldlg.src#RID_TP_LIBS.RID_PB_PASSWORD.pushbutton.text
msgid "~Password..."
msgstr "~كلمة المرور..."

#: moduldlg.src#RID_TP_LIBS.RID_PB_APPEND.pushbutton.text
msgid "~Append..."
msgstr "~إلحاق..."

#: moduldlg.src#RID_DLG_LIBS.RID_FL_OPTIONS.fixedline.text
msgid "Options"
msgstr "خيارات"

#: moduldlg.src#RID_DLG_LIBS.RID_CB_REF.checkbox.text
msgid "Insert as reference (read-only)"
msgstr "إدراج كمرجع (للقراءة فقط)"

#: moduldlg.src#RID_DLG_LIBS.RID_CB_REPL.checkbox.text
msgid "Replace existing libraries"
msgstr "استبدال المكتبات الموجودة"

#: moduldlg.src#RID_DLG_NEWLIB.RID_FT_NEWLIB.fixedtext.text
msgid "~Name:"
msgstr "~الاسم:"

#: moptions.src#RID_MACROOPTIONS.RID_FT_DESCR.fixedtext.text moptions.src#RID_MACROOPTIONS.modaldialog.text
msgid "Description"
msgstr "الوصف"

#: moptions.src#RID_MACROOPTIONS.RID_FL_HELP.fixedline.text
msgid "Help information"
msgstr "معلومات عن المساعدة"

#: moptions.src#RID_MACROOPTIONS.RID_FT_HELPID.fixedtext.text
msgid "Help ID"
msgstr "تعريف المساعدة"

#: moptions.src#RID_MACROOPTIONS.RID_FT_HELPNAME.fixedtext.text
msgid "Help file name"
msgstr "اسم ملف المساعدة"

#: objdlg.src#RID_BASICIDE_OBJCAT.RID_TB_TOOLBOX.TBITEM_SHOW.toolboxitem.text
msgid "Show"
msgstr "إظهار"

#: objdlg.src#RID_BASICIDE_OBJCAT.floatingwindow.text
msgid "Objects"
msgstr "كائنات"

#: tbxctl.src#RID_TBXCONTROLS.RID_TOOLBOX.string.text tbxctl.src#RID_TBXCONTROLS.floatingwindow.text
msgid "Controls"
msgstr "عناصر تحكم"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_PUSHBUTTON.toolboxitem.text
msgid "Button"
msgstr "مفتاح"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_CHECKBOX.toolboxitem.text
msgid "Check Box"
msgstr "خانة اختيار"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_FIXEDTEXT.toolboxitem.text
msgid "Label field"
msgstr "بطاقة عنونة"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_LISTBOX.toolboxitem.text
msgid "List Box"
msgstr "مربع قائمة"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_HSCROLLBAR.toolboxitem.text
msgid "Horizontal Scroll Bar"
msgstr "شريط تحريك أفقي"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_GROUPBOX.toolboxitem.text
msgid "Group Box"
msgstr "صندوق تجميع"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_HFIXEDLINE.toolboxitem.text
msgid "Horizontal Line"
msgstr "خط أفقي"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_DATEFIELD.toolboxitem.text
msgid "Date Field"
msgstr "حقل التاريخ"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_NUMERICFIELD.toolboxitem.text
msgid "Numeric Field"
msgstr "حقل رقمي"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_FORMATTEDFIELD.toolboxitem.text
msgid "Formatted Field"
msgstr "حقل مُنسَّق"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_FILECONTROL.toolboxitem.text
msgid "File Selection"
msgstr "اختيار الملف"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_IMAGECONTROL.toolboxitem.text
msgid "Image Control"
msgstr "عنصر تحكم مرسوم"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_RADIOBUTTON.toolboxitem.text
msgid "Option Button"
msgstr "زر خيار"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_EDIT.toolboxitem.text
msgid "Text Box"
msgstr "حقل نص"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_COMBOBOX.toolboxitem.text
msgid "Combo Box"
msgstr "مربع تحرير وسرد"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_VSCROLLBAR.toolboxitem.text
msgid "Vertical Scroll Bar"
msgstr "شريط تحريك رأسي"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_PROGRESSBAR.toolboxitem.text
msgid "Progress Bar"
msgstr "شريط التقدم"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_VFIXEDLINE.toolboxitem.text
msgid "Vertical Line"
msgstr "خط عمودي"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_TIMEFIELD.toolboxitem.text
msgid "Time Field"
msgstr "حقل الوقت"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_CURRENCYFIELD.toolboxitem.text
msgid "Currency Field"
msgstr "حقل العملة"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_PATTERNFIELD.toolboxitem.text
msgid "Pattern Field"
msgstr "حقل مُقنّع"

#: tbxctl.src#RID_TOOLBOX.SID_INSERT_SELECT.toolboxitem.text
msgid "Select"
msgstr "اختيار"

#: tbxctl.src#RID_TOOLBOX.SID_DIALOG_TESTMODE.toolboxitem.text
msgid "Activate Test Mode"
msgstr "تنشيط وضع الاختبار"

