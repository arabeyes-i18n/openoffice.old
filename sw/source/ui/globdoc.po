# translation of globdoc.po to Arabic
# extracted from sw/source/ui/globdoc.oo
# Ossama M. Khayat <okhayat@yahoo.com>, 2004, 2005.
msgid ""
msgstr ""
"Project-Id-Version: globdoc\n"
"POT-Creation-Date: 2002-07-15 17:13+0100\n"
"PO-Revision-Date: 2005-01-18 12:58+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#: globdoc.src#STR_HUMAN_SWGLOBDOC_NAME.string.text
msgid "Master Document"
msgstr "مستند شامل"

#: globdoc.src#STR_WRITER_GLOBALDOC_FULLTYPE.string.text
msgid "%PRODUCTNAME %PRODUCTVERSION Master Document"
msgstr "مستند %PRODUCTNAME %PRODUCTVERSION شامل"
