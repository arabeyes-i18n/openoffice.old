#!/bin/sh

for file in `find -type f -name *.po`
do
  echo -n $file:
  msgfmt -c --statistics $file
done 
