#!/bin/bash

#
# This script can be used to create/update PO files from OpenOffice.org POT
# files.
#

# Please send comment to Pavel Jan�k

#
# This script expects:
#
# - POT files in pot directory (as unpacked from latest POT files)
#
# - PO files in po directory
#
# If you do not have PO files in po directory, this script will create
# empty PO files there instead.
#

for POT in `find pot -type f`
do
   PO=`echo $POT|sed 's#pot/#po/#'|sed 's#.pot#.po#'`
   echo $PO
   # Create directory for new PO file
   mkdir -p `dirname $PO`
   if [ ! -f $PO ]
   then
     msginit --no-translator -i $POT -o $PO
   else
     msgmerge --backup=off -w 200 --no-fuzzy-matching -U $PO $POT
   fi
done

for PO in `find po -type f -name *.po`
do
     POT=`echo $PO|sed 's#^po/#pot/#'|sed 's#.po$#.pot#'`

     [ ! -f $POT ] && {
	 echo "POT file $POT was removed, rename $PO"
     }
done


#
# Revision history:
#
# 1.2 (Thu Dec  9 12:03:46 CET 2004)
#
# - add --no-fuzzy-matching
#
# 1.1 (Wed Dec  1 20:27:07 CET 2004)
#
# - warn about removed POT files
#
# 1.0 (Sun Nov 28 22:00:55 CET 2004)
#
# - first public version
#
